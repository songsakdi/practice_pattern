/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.revisejavamavenproject1;

/**
 *
 * @author User
 */
public enum DayOfWeek {
    SUNDAY(7),
    MONDAY(1),
    TUESDAY(2),
    WEDNESDAY(3),
    THURSDAY(4),
    FRIDAY(5),
    SATURDAY(6);
    
    private int value ;


    private DayOfWeek(int value) {
        this.value = value;
    }

    public int getValue() {
	    return this.value;
    }

         
}
